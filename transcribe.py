import os
import shutil
import torch
import gc
import ffmpeg
import glob
from pytube import YouTube
from whisper import load_model
from whisper.audio import SAMPLE_RATE
from .vad import load_vad_model
from .asr import transcribe_with_vad

HUGGING_FACE_TOKEN: str = ""
TEMP_PATH = "temp_files"

def download_youtube_videos():
    output_path = "temp_files"

    #videos = [  "https://www.youtube.com/watch?v=yTROqe8T_eA",
                #"https://www.youtube.com/watch?v=_PO5NyOve6M",
                #"https://www.youtube.com/watch?v=mq9lBd8XMY4",
                #"https://www.youtube.com/watch?v=kT_-qUxrlOU&t",
                #"https://www.youtube.com/watch?v=vIvWwrvcTkc",
                #"https://www.youtube.com/watch?v=hxLypJ-sAy4",
                #"https://www.youtube.com/watch?v=FN3yxL0Rr0c",
                #"https://www.youtube.com/watch?v=-tE0UqE1R8E",
                #"https://www.youtube.com/watch?v=JBKuRPSaou0",
                #"https://www.youtube.com/watch?v=3RvKEundVps",
                #"https://www.youtube.com/watch?v=Kfr_FZof_hs"
            #]

    videos = ["https://www.youtube.com/watch?v=eYmaUY0rbtk",
              "https://www.youtube.com/watch?v=AxNc7rGmGAQ",
              "https://www.youtube.com/watch?v=Eol5Na0PDnM",
              "https://www.youtube.com/watch?v=rUqZHSK7Rt4"]

    for index, video_url in enumerate(videos):
        YouTube(video_url) \
        .streams \
        .filter(audio_codec="mp4a.40.2") \
        .first() \
        .download(output_path=TEMP_PATH, filename=f'video{index}.mp4')


def convert_audio_file_to_wav_file(audio_path):
    audio_basename = os.path.splitext(os.path.basename(audio_path))[0]
    input_audio_path = os.path.join(TEMP_PATH, audio_basename + ".wav")
    ffmpeg.input(audio_path, threads=0).output(input_audio_path, ac=1, ar=SAMPLE_RATE).run(cmd=["ffmpeg"])


def convert_all_audio_files_to_wav_files():
    for audio_file in glob.glob(TEMP_PATH + "/*"):
        convert_audio_file_to_wav_file(audio_file)


def transcribe_audio():
    device: str = "cuda" if torch.cuda.is_available() else "cpu"
    vad_model = load_vad_model(torch.device(device), vad_onset= 0.767, vad_offset=0.363, use_auth_token=HUGGING_FACE_TOKEN)
    model = load_model("small", device=device, download_root="./models")
    results = []
    for audio_path in glob.glob(TEMP_PATH + "/*.wav"):
        input_audio_path = audio_path
        result = transcribe_with_vad(model, input_audio_path, vad_model, temperature=0)
        results.append((result, input_audio_path))

    # Unload Whisper and VAD
    del model
    del vad_model
    gc.collect()
    torch.cuda.empty_cache()

    return results


def generated_audio_samples(transcribed_audio):
    wav_folder = TEMP_PATH + "/wavs"

    # ToDo 
    wav = ""
    transcription = ""

    # append audio sample with transcription to summarazing text file
    with open(TEMP_PATH + "/all_wavs.txt", "a") as text_file:
        text_file.write('wavs/' + wav + '.wav|' + transcription + '\n')


def create_train_val_split():
    with open(TEMP_PATH + "/all_wavs.txt") as f:
        all_audio_samples = f.readlines()
    audio_sample_list = all_audio_samples.strip().split("\n")
    for audio_sample in audio_sample_list:
        pass

if __name__ == "__main__":
    if os.path.exists(TEMP_PATH):
        shutil.rmtree(TEMP_PATH)
    os.makedirs(TEMP_PATH)

    download_youtube_videos()
    convert_all_audio_files_to_wav_files()
    transcribed_audio = transcribe_audio()
    generated_audio_samples(transcribed_audio)
    create_train_val_split()
    print(SAMPLE_RATE)
    

    # ToDo Make sure audio is in 22.05KHz