from typing import TypedDict, Optional


class TranscriptionResult(TypedDict):
    """
    A list of segments and word segments of a speech.
    """
    segments: list[SingleSegment]
    language: str


class SingleSegment(TypedDict):
    """
    A single segment (up to multiple sentences) of a speech.
    """

    start: float
    end: float
    text: str